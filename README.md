
# Apereo CAS Ansible Deployment
https://apereo.github.io/cas/6.4.x/

CAS version 6.4.x, using OpenJDK 11.

# Development

This project is an Ansible role for use  in other projects and uses the Ansible
Molecule framework for test-driven development of Ansible roles.  Specifically we
are using the Vagrant driver, which in turn uses the KVM hypervisor.

Read more about the Ansible Molecule framework here:
https://molecule.readthedocs.io/en/latest/

These projects will install and configure Vagrant and Molecule on Fedora 34:
- Vagrant: https://gitlab.davenport.edu/demo/vagrant
- Molecule: https://gitlab.davenport.edu/demo/molecule

The installation and basic workflow is as follows:
```bash
molecule create
# do some Ansible development
molecule converge
molecule verify
# access the VM to troubleshoot issues
molecule login
# GOTO do some Ansible development
molecule destroy
# run a full run from create to destroy, with a few addional checks
molecule test
# add changes to git, commit, and push
```

# Authentication

## Permissive Authentication
https://apereo.github.io/cas/6.4.x/authentication/Permissive-Authentication.html

This covers local credential stores either in the configuration or in a file
stored on the server.  

Hard-coded credentials:  
- defaults/main.yml: config_cas_authn_accept_*
- templates/etc/config/cas.properties: cas.authn.accept.*

Credentials and user attributes stored in a JSON file:  
- defaults/main.yml: config_cas_authn_json_*
- templates/etc/config/cas.properties: cas.authn.json.*

## Simple Multi-Factor Authentication (MFA) using email
https://apereo.github.io/cas/6.4.x/mfa/Simple-Multifactor-Authentication.html

MFA using One-Time Password (OTP) to a users email.  SMS is also available using
this component but is not implemented or tested for our deployment.

- defaults/main.yml: config_cas_authn_mfa_simple_mail_*
- templates/etc/config/cas.properties: cas.authn.mfa.simple.*

# Service Provider (SP) Configuration

## JSON SP Configuration
https://apereo.github.io/cas/6.4.x/services/JSON-Service-Management.html

Load Service Provider configurations from a directory of JSON files.
- defaults/main.yml: config_cas_serviceRegistry_json_*
- templates/etc/config/cas.properties: cas.serviceRegistry.json.*

# Protocols

The CAS server supports multiple protocols.  By default, CAS version 1, 2, and 3
are enabled.  The SAML 2.0 protocol is also enable in our implementation.

## SAML 2.0
https://apereo.github.io/cas/6.4.x/authentication/Configuring-SAML2-Authentication.html

The SAML 2,0 protocol is preferred when it is supported by the SP.  Each SAML SP
requires an SP config.  See molecule tests for an example.  The SAML protocol
requires key pairs for signing, encryption, and other support files.  The default
directory for those files is /etc/cas/saml.  If you do not supply your own keys,
CAS will attempt to create them by default.  The CAS service user does not have
write access to the correct folder in our implementation, therefore you must
modify the permissions for CAS to write to the folder, or supply your own keys.

- defaults/main.yml: config_cas_authn_samlIdp_*
- templates/etc/config/cas.properties: cas.authn.samlIdp.*

# Administration Endpoints

The CAS server and underlying software provides various web-accessible endpoints
for managing and monitoring the application.  These endpoints are generally
accessible at `/cas/actuator/*` and you should be able to see a list of the enabled
endpoints by visiting `/cas/actuator/` in a web browser.

No endpoints are enabled by default, but they can be enabled using a variable:  
- defaults/main.yml: config_management_endpoints
- templates/etc/config/cas.properties:
  - management.endpoint*
  - cas.monitor.endpoints.*

Endpoints of interest and currently confirmed to work include:  
- /cas/actuator/info
- /cas/actuator/health
- /cas/actuator/metrics - [docs](https://apereo.github.io/cas/6.4.x/monitoring/Configuring-Metrics.html)
- /cas/actuator/metrics/*
- /cas/actuator/statistics
- /cas/actuator/registeredServices - [docs](https://apereo.github.io/cas/6.4.x/services/Service-Management.html#actuator-endpoints)

## Monitoring & Stats
https://apereo.github.io/cas/6.4.x/monitoring/Monitoring-Statistics.html
