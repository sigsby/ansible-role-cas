
# Running containers using systemd as a non-root user

https://blog.christophersmart.com/2021/02/20/rootless-podman-containers-under-system-accounts-managed-and-enabled-at-boot-with-systemd/

# Theme

## List template view files

```bash
cd /usr/src/cas
sudo ./gradlew listTemplateViews
ls /usr/src/cas/build/cas-resources/templates
```

## Edit template file

Add any views that require customization to the src/main/resources/templates folder in the CAS overlay project.

```bash
cd /usr/src/cas
sudo ./gradlew getResource -PresourceName=header.html
sudo nano /usr/src/cas/src/main/resources/templates/fragments/header.html
sudo ./gradlew build
sudo systemctl restart cas
```

## Copy customization to build

Copy the files extracted from the views to these folders and they will be copied into the next build:

  cas/files/templates
  cas/files/static

Theme.yml is imported in main.yml after git pulling the overlay project

  tasks/theme.yml

# Links

Forgot your password?

  files/templates/fragments/pmlinks.html

Need tech support?

  files/templates/fragments/loginsidebar.html

# Todo
- Customize other views? Which files?
- Customize error messages?
- Open links in new tab?
